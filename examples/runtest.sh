#!/bin/bash
# this file should not be called directly from the shell,
# it ought to be included from within run.sh for each case

if [[ -x ./waspy ]] && [[ ! -h ./waspy ]]; then
 waspybin=./waspy
 testdir="examples/"
elif [[ -x ../waspy ]] && [[ ! -d ../waspy ]]; then
 waspybin="../waspy"
 testdir="./"
elif [ ! -z "`which wasora`" ]; then
 if [ -x ./waspy.so ]; then
  waspybin="wasora -p waspy.so"
  testdir="examples/"
 elif [ -x ../waspy.so ]; then
  waspybin="wasora -p ../waspy.so"
  testdir=""
 fi
else
 echo "do not know how to run waspy :("
 exit 1
fi
