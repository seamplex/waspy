waspy: execute python code within wasora
========================================

See the `examples` subdirectory.


Further information
-------------------

Home page: <http://www.talador.com.ar/jeremy/wasora>  
Mailing list and bug reports: <wasora@talador.com.ar>  


wasora is copyright (C) 2009--2014 jeremy theler  
wasora is licensed under [GNU GPL version 3](http://www.gnu.org/copyleft/gpl.html) or (at your option) any later version.  
wasora is free software: you are free to change and redistribute it.  
There is NO WARRANTY, to the extent permitted by law.  
See the file `COPYING` for copying conditions.  
