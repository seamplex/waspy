/*------------ -------------- -------- --- ----- ---   --       -            -
 *  waspy plugin for wasora
 *
 *  Copyright (C) 2013 esteban pellegrino, 2014--2017 jeremy theler
 *
 *  This file is part of waspy.
 *
 *  waspy is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  waspy is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with wasora.  If not, see <http://www.gnu.org/licenses/>.
 *------------------- ------------  ----    --------  --     -       -         -
 */

// For PYTHON interface
#include <python2.7/Python.h>

#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <string>
#include <cstring>

extern "C" {
  #include <wasora.h>
}

const std::string trim(const std::string& pString,const std::string& pWhitespace) {
    const std::string::size_type beginStr = pString.find_first_not_of(pWhitespace);
    if (beginStr == std::string::npos) {
        // No content
        return "";
    }
    const std::string::size_type endStr = pString.find_last_not_of(pWhitespace);
    const std::string::size_type range = endStr - beginStr + 1;
    return pString.substr(beginStr, range);
}

const std::string reduce(const std::string& pString,const std::string& pFill,const std::string& pWhitespace) {
    // Trim first
    std::string result(trim(pString, pWhitespace));

    // Replace sub ranges
    std::string::size_type beginSpace = result.find_first_of(pWhitespace);
    while (beginSpace != std::string::npos) {
        const std::string::size_type endSpace = result.find_first_not_of(pWhitespace, beginSpace);
        const std::string::size_type range = endSpace - beginSpace;
        result.replace(beginSpace, range, pFill);
        const std::string::size_type newStart = beginSpace + pFill.length();
        beginSpace = result.find_first_of(pWhitespace, newStart);
    }
    return result;
}

/* Token */
template<class Container>
void tokenize(const std::string& str, Container& tokens, const std::string& delimiters = " ") {
  /* Skip delimiters at beginning */
  std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
  /* Find first non-delimiter */
  std::string::size_type pos = str.find_first_of(delimiters, lastPos);

  while (std::string::npos != pos || std::string::npos != lastPos) {
    /* Found a token, add it to the vector */
    tokens.push_back(str.substr(lastPos, pos - lastPos));
    /* Skip delimiters */
    lastPos = str.find_first_not_of(delimiters, pos);
    /* Find next non-delimiter */
    pos = str.find_first_of(delimiters, lastPos);
  }
}

// Class to save the PYTHON code information
class PythonCode {
  // Code
  std::string _code;
public:
  PythonCode(const std::string& code) : _code(code) {}

  // Get code
  const char* getCode() const {
    return _code.c_str();
  }

  PythonCode() {}
};

// Map of calls names and code
static std::map<std::string, PythonCode*> python_map;
// Container of inline calls
static std::vector<PythonCode*> python_inline;

// instruction, as defined below with wasora_define_instruction()
extern "C" int execute_python(void *arg);

// initialization before parsing the input file
// like checking that everything is OK, installing error
// handlers, initializing libraries, defining internal objects, etc
extern "C" int plugin_init_before_parser(void) {
  // Optional but recommended
  Py_SetProgramName(wasora.argv[0]);

  // Initialize PYTHON stuff
  Py_Initialize();

  // Set command line arguments
//  PySys_SetArgv(wasora.argc_unknown, wasora.argv_unknown);
  PySys_SetArgv(wasora.argc_orig, wasora.argv_orig);

  return WASORA_PARSER_OK; 
}

// parse a line of the input file
// only those lines not understood by wasora are passed
// to the plugins
extern "C" int plugin_parse_line(char *c_line) {
  char *token;

  // Get line as a STL string
  std::string line(c_line);
  // Get tokens
  std::vector<std::string> tokens;
  tokenize(line, tokens);

  if(tokens[0] == "PYTHON") {
    if(not tokens.size() >= 2) {
      wasora_push_error_message("expected PYTHON script definition");
      return WASORA_PARSER_ERROR;
    } else {
      std::string name = tokens[1];
      PythonCode* python_code(0);

      // Find PYTHON code
      size_t code_pos = line.find("CODE");
      if(code_pos != std::string::npos) {
        python_code = new PythonCode(line.substr(code_pos + 4));
      } else {
        wasora_push_error_message("missing CODE keyword in PYTHON script definition");
        return WASORA_PARSER_ERROR;
      }

      if(name == "INLINE") {
        // Is an inline call, put an instruction here (and register to the anonymous calls container)
        python_inline.push_back(python_code);
        wasora_define_instruction(execute_python, reinterpret_cast<void*>(python_code));
      } else {
        // Register the call into the global map
        python_map.insert(make_pair(name, python_code));
      }
      return WASORA_PARSER_OK;

    }
  } else if(tokens[0] == "PYTHON_CALL") {
    if(tokens.size() >= 2) {
      std::string routine_name(tokens[1]);
      std::map<std::string, PythonCode*>::const_iterator it = python_map.find(routine_name);
      if(it != python_map.end()) {
        wasora_define_instruction(execute_python, reinterpret_cast<void*>((*it).second));
        return WASORA_PARSER_OK;
      } else {
        wasora_push_error_message("PYTHON script %s is not defined", routine_name.c_str());
        return WASORA_PARSER_ERROR;
      }
    } else {
      wasora_push_error_message("expected PYTHON script's name");
      return WASORA_PARSER_ERROR;
    }
  } else {
    return WASORA_PARSER_UNHANDLED;
  }

  return WASORA_PARSER_OK; 
}

// initialization after parsing the input file
// like checking consistency of the input, allocating dynamic
// resources, etc
extern "C" int plugin_init_after_parser(void) {
  return WASORA_PARSER_OK;
}

// initialization before running
// like resetting time, histories, meshes, etc
// each time a standard run is executed, this routine is called
extern "C" int plugin_init_before_run(void) {
  return WASORA_RUNTIME_OK;
}

// finalization
extern "C" int plugin_finalize(void) {
  // Eliminate allocated calls
  for(std::map<std::string, PythonCode*>::const_iterator it = python_map.begin() ; it != python_map.end() ; ++it)
    delete (*it).second;
  // Eliminate inline calls
  for(std::vector<PythonCode*>::const_iterator it = python_inline.begin() ; it != python_inline.end() ; ++it)
    delete (*it);
  Py_Finalize();
  return WASORA_RUNTIME_OK;
}

// instruction, as defined above with wasora_define_instruction()
extern "C" int execute_python(void *arg) {
  // Access main module and register variables
  PyObject* py_main = PyImport_AddModule("__main__");
  PyObject *globals = PyModule_GetDict(py_main);

  // Map variables to python
  var_t *var = wasora.vars;
  while (var != NULL) {
    PyObject* pydouble = PyFloat_FromDouble(wasora_value(var));
    PyDict_SetItemString(globals, var->name, pydouble);
    Py_DECREF(pydouble);
    var = (var_t *)var->hh.next;
  }

  // Map matrices to PYTHON
  matrix_t *mats = wasora.matrices;
  while (mats != NULL) {
    if (!mats->initialized) {
      wasora_call(wasora_matrix_init(mats));
    }
    PyObject* pymatrix = PyList_New(mats->rows);
    for(int i = 0 ; i < mats->rows ; ++i) {
      PyObject* columns = PyList_New(mats->cols);
      for(int j = 0 ; j < mats->cols ; ++j) {
        PyList_SetItem(columns, j, PyFloat_FromDouble(gsl_matrix_get(wasora_value_ptr(mats), i, j)));
      }
      PyList_SetItem(pymatrix, i, columns);
    }
    PyDict_SetItemString(globals, mats->name, pymatrix);
    Py_DECREF(pymatrix);
    mats = (matrix_t *)mats->hh.next;
  }

  // Map vectors to python
  vector_t *vector = wasora.vectors;
  while (vector != NULL) {
    if (!vector->initialized) {
      wasora_call(wasora_vector_init(vector));
    }
    PyObject* pyvector = PyList_New(vector->size);
    for(int i = 0 ; i < vector->size ; ++i)
      PyList_SetItem(pyvector, i, PyFloat_FromDouble(gsl_vector_get(wasora_value_ptr(vector), i)));
    PyDict_SetItemString(globals, vector->name, pyvector);
    Py_DECREF(pyvector);
    vector = (vector_t *)vector->hh.next;
  }

  // Cast point to call the python execute function
  const PythonCode* python_code = reinterpret_cast<const PythonCode*>(arg);

  // Execute code inside the file
  if(PyRun_SimpleString(python_code->getCode())) {
    wasora_push_error_message("executing python code");
    return WASORA_RUNTIME_ERROR;  
  }

  // Map variables back to wasora
  var = wasora.vars;
  while (var != NULL) {
     PyObject* pydouble = PyDict_GetItemString(globals, var->name);
     if (pydouble != NULL) {
       wasora_value(var) = PyFloat_AsDouble(pydouble);
     }
     var = (var_t *)var->hh.next;
  }
  
  // Map matrices back to wasora
  mats = wasora.matrices;
  while (mats != NULL) {
    PyObject* pymatrix = PyDict_GetItemString(globals, mats->name);
    for(int i = 0 ; i < mats->rows ; ++i) {
      PyObject* columns =PyList_GetItem(pymatrix, i);
      for(int j = 0 ; j < mats->cols ; ++j)
        gsl_matrix_set(wasora_value_ptr(mats), i, j, PyFloat_AsDouble(PyList_GetItem(columns, j)));
    }
    mats = (matrix_t *)mats->hh.next;
  }

  // Map vectors back to wasora
  vector = wasora.vectors;
  while (vector != NULL) {
    PyObject* pyvector = PyDict_GetItemString(globals, vector->name);
    for(int i = 0 ; i < vector->size ; ++i)
      gsl_vector_set(wasora_value_ptr(vector), i, PyFloat_AsDouble(PyList_GetItem(pyvector, i)));
    vector = (vector_t *)vector->hh.next;
  }

  return WASORA_RUNTIME_OK;
}
